
from enum import Enum
from lang.assembly.isa import *


class Register(str, Enum):
    AC = 'acc'
    IP = 'ip'
    DR = 'dr'
    SP = 'sp'
    INP = 'inp'
    OUTP = 'outp'


def do_add(x, y):
    return x + y


def do_sub(x, y):
    return x - y


def do_mul(x, y):
    return x * y


def do_div(x, y):
    return x // y


def do_mod(x, y):
    return x % y


def do_setz(x, y):
    return 1 if x == y else 0


def do_setnz(x, y):
    return 1 if x != y else 0


def do_setg(x, y):
    return 1 if x > y else 0


def do_setge(x, y):
    return 1 if x >= y else 0


def do_setl(x, y):
    return 1 if x < y else 0


def do_setle(x, y):
    return 1 if x <= y else 0


def do_and(x, y):
    return 1 if x and y else 0


def do_or(x, y):
    return 1 if x or y else 0


stack_operations_performers = {
    Opcode.ADDST: do_add,
    Opcode.SUBST: do_sub,
    Opcode.MULST: do_mul,
    Opcode.DIVST: do_div,
    Opcode.MODST: do_mod,

    Opcode.SETZST: do_setz,
    Opcode.SETNZST: do_setnz,
    Opcode.SETGST: do_setg,
    Opcode.SETGEST: do_setge,
    Opcode.SETLST: do_setl,
    Opcode.SETLEST: do_setle,

    Opcode.ANDST: do_and,
    Opcode.ORST: do_or
}


def check_jump(_):
    return True


def check_jz(acc):
    return acc == 0


def check_jnz(acc):
    return acc != 0


jump_operations = {
    Opcode.JMP: check_jump,
    Opcode.JZ: check_jz,
    Opcode.JNZ: check_jnz
}

MEMORY_SIZE = 65536


def log(log_str: str, nest_level=0, do_log=True):
    if do_log:
        print(('\t' * nest_level) + log_str)


def execute(instructions: list[Instruction], memory_size=MEMORY_SIZE, input_file=None, do_log=False, dump=False):
    segment_size = memory_size // 4
    data_size = memory_size // 2
    input_segment = segment_size * 2
    output_segment = segment_size * 3

    registers: dict[str, int] = dict()

    registers[Register.AC] = 0
    registers[Register.IP] = 0
    registers[Register.DR] = 0
    registers[Register.SP] = 0
    registers[Register.INP] = input_segment
    registers[Register.OUTP] = output_segment

    mem = [0] * memory_size

    if input_file is not None:
        with open(input_file) as f:
            in_reg = registers[Register.INP]
            written_symbols = 0

            for symbol in f.read():
                mem[in_reg] = ord(symbol)
                in_reg += 1
                written_symbols += 1

                if written_symbols < segment_size:
                    mem[in_reg] = -1
                else:
                    break

    while registers[Register.IP] < len(instructions):
        instr = instructions[registers[Register.IP]]
        registers[Register.IP] += 1

        if do_log:
            print_instruction(instr)

        if isinstance(instr, NonArgInstruction):
            if instr.opcode == Opcode.PUSH:
                registers[Register.SP] = (registers[Register.SP] - 1) % data_size
                stack_pointer = registers[Register.SP]
                mem[stack_pointer] = registers[Register.AC]
            elif instr.opcode == Opcode.POP:
                stack_pointer = registers[Register.SP]
                registers[Register.AC] = mem[stack_pointer]
                registers[Register.SP] = (stack_pointer + 1) % data_size
            elif instr.opcode in stack_operations_performers.keys():
                stack_pointer = registers[Register.SP]
                registers[Register.AC] = mem[stack_pointer]
                registers[Register.DR] = mem[stack_pointer + 1]
                registers[Register.AC] = stack_operations_performers[instr.opcode](registers[Register.AC], registers[Register.DR])

                mem[stack_pointer + 1] = registers[Register.AC]
                registers[Register.SP] = (stack_pointer + 1) % data_size
            elif instr.opcode == Opcode.PUT:
                out_pointer = registers[Register.OUTP]

                mem[out_pointer] = registers[Register.AC]
                out_pointer = registers[Register.OUTP] + 1

                if out_pointer == memory_size:
                    registers[Register.OUTP] = output_segment
                else:
                    registers[Register.OUTP] = out_pointer

                print(chr(registers[Register.AC]), end='')
            elif instr.opcode == Opcode.GET:
                in_pointer = registers[Register.INP]

                if in_pointer == registers[Register.OUTP] or mem[in_pointer] == -1:
                    registers[Register.AC] = -1
                else:
                    registers[Register.AC] = mem[in_pointer]
                    registers[Register.INP] = in_pointer + 1

                    # print('AC:', registers[Register.AC])
        elif isinstance(instr, ArgInstruction):
            instr_arg_type = instr.get_arg_value_type()
            instr_arg = instr.get_arg_value()

            if instr.opcode == Opcode.LD:
                if instr_arg_type == ValueType.IMMEDIATE:
                    registers[Register.AC] = instr_arg
                elif instr_arg_type == ValueType.DIRECT:
                    registers[Register.AC] = mem[instr_arg]
                elif instr_arg_type == ValueType.STACK:
                    stack_pointer = registers[Register.SP]
                    registers[Register.AC] = mem[stack_pointer + instr_arg]
            elif instr.opcode == Opcode.ST:
                if instr_arg_type == ValueType.DIRECT:
                    mem[instr_arg] = registers[Register.AC]
                elif instr_arg_type == ValueType.STACK:
                    stack_pointer = registers[Register.SP]
                    mem[stack_pointer + instr_arg] = registers[Register.AC]
            elif instr.opcode == Opcode.ISP:
                stack_pointer = registers[Register.SP]
                registers[Register.SP] = (stack_pointer + instr_arg) % data_size
            elif instr.opcode == Opcode.DSP:
                stack_pointer = registers[Register.SP]
                registers[Register.SP] = (stack_pointer - instr_arg) % data_size
            elif instr.opcode in jump_operations.keys():
                if jump_operations[instr.opcode](registers[Register.AC]):
                    if instr_arg_type == ValueType.DIRECT:
                        registers[Register.IP] = instr_arg
                    elif instr_arg_type == ValueType.INDIRECT:
                        registers[Register.IP] += instr_arg
                    elif instr_arg_type == ValueType.STACK:
                        registers[Register.IP] = registers[Register.SP] + instr_arg

        if do_log:
            print('Registers dump:', registers)
            print('Memory dump:')

            for idx, val in enumerate(mem):
                print(f'MEM[{idx}] = {val}')

            print()

    if dump:
        print('Registers dump:', registers)
        print('Memory dump:')

        for idx, val in enumerate(mem):
            print(f'MEM[{idx}] = {val}')

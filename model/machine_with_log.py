#
# from enum import Enum
# from lang.assembly.isa import *
#
#
# class Register(str, Enum):
#     AC = 'acc'
#     IP = 'ip'
#     DR = 'dr'
#     SP = 'sp'
#
#
# def do_add(x, y):
#     return x + y
#
#
# def do_sub(x, y):
#     return x - y
#
#
# def do_mul(x, y):
#     return x * y
#
#
# def do_div(x, y):
#     return x // y
#
#
# def do_mod(x, y):
#     return x % y
#
#
# def do_setz(x, y):
#     return 1 if x == y else 0
#
#
# def do_setnz(x, y):
#     return 1 if x != y else 0
#
#
# def do_setg(x, y):
#     return 1 if x > y else 0
#
#
# def do_setge(x, y):
#     return 1 if x >= y else 0
#
#
# def do_setl(x, y):
#     return 1 if x < y else 0
#
#
# def do_setle(x, y):
#     return 1 if x <= y else 0
#
#
# def do_and(x, y):
#     return x and y
#
#
# def do_or(x, y):
#     return x or y
#
#
# stack_operations_performers = {
#     Opcode.ADDST: do_add,
#     Opcode.SUBST: do_sub,
#     Opcode.MULST: do_mul,
#     Opcode.DIVST: do_div,
#     Opcode.MODST: do_mod,
#
#     Opcode.SETZST: do_setz,
#     Opcode.SETNZST: do_setnz,
#     Opcode.SETGST: do_setg,
#     Opcode.SETGEST: do_setge,
#     Opcode.SETLST: do_setl,
#     Opcode.SETLEST: do_setle,
#
#     Opcode.ANDST: do_and,
#     Opcode.ORST: do_or
# }
#
# MEMORY_SIZE = 65536
# DATA_SIZE = MEMORY_SIZE // 2 # 32768
#
#
# def log(log_str: str, nest_level=0, do_log=True):
#     if do_log:
#         print(('\t' * nest_level) + log_str)
#
#
# def print_instruction(instr):
#     if isinstance(instr, ArgInstruction):
#         print(instr.get_opcode().value, ' ', sep='', end='')
#
#         prefix = ''
#
#         if instr.get_arg_value_type() == ValueType.STACK:
#             prefix = '&'
#         elif instr.get_arg_value_type() == ValueType.IMMEDIATE:
#             prefix = '#'
#         elif instr.get_arg_value_type() == ValueType.DIRECT:
#             prefix = '$'
#
#         print(f"{prefix}{instr.get_arg_value()}")
#     elif isinstance(instr, NonArgInstruction):
#         print(instr.get_opcode().value)
#
#
# def execute(instructions: list[Instruction], do_log=False):
#     registers: dict[str, int] = dict()
#
#     registers[Register.AC] = 0
#     registers[Register.IP] = 0
#     registers[Register.DR] = 0
#     registers[Register.SP] = 0
#
#     mem = [0] * MEMORY_SIZE
#
#     while registers[Register.IP] < len(instructions):
#         instr = instructions[registers[Register.IP]]
#         registers[Register.IP] += 1
#
#         if do_log:
#             print_instruction(instr)
#
#         if isinstance(instr, NonArgInstruction):
#             if instr.opcode == Opcode.PUSH:
#                 log(f'SP: {registers[Register.SP]}', nest_level=1, do_log=do_log)
#                 registers[Register.SP] = (registers[Register.SP] - 1) % DATA_SIZE
#                 log(f'SP: {registers[Register.SP]}', nest_level=1, do_log=do_log)
#
#                 stack_pointer = registers[Register.SP]
#
#                 log(f'AC: {registers[Register.AC]}, MEM[...] = {mem[stack_pointer]}', nest_level=1, do_log=do_log)
#                 mem[stack_pointer] = registers[Register.AC]
#                 log(f'AC: {registers[Register.AC]}, MEM[...] = {mem[stack_pointer]}', nest_level=1, do_log=do_log)
#             elif instr.opcode == Opcode.POP:
#                 stack_pointer = registers[Register.SP]
#
#                 log(f'AC: {registers[Register.AC]}, MEM[...] = {mem[stack_pointer]}', nest_level=1, do_log=do_log)
#                 registers[Register.AC] = mem[stack_pointer]
#                 log(f'AC: {registers[Register.AC]}, MEM[...] = {mem[stack_pointer]}', nest_level=1, do_log=do_log)
#
#                 log(f'SP: {registers[Register.SP]}', nest_level=1, do_log=do_log)
#                 registers[Register.SP] = (stack_pointer + 1) % DATA_SIZE
#                 log(f'SP: {registers[Register.SP]}', nest_level=1, do_log=do_log)
#             elif instr.opcode in stack_operations_performers.keys():
#                 stack_pointer = registers[Register.SP]
#                 # acc = registers[Register.AC]
#                 registers[Register.AC] = mem[stack_pointer]
#                 registers[Register.DR] = mem[stack_pointer + 1]
#                 registers[Register.AC] = stack_operations_performers[instr.opcode](registers[Register.AC], registers[Register.DR])
#
#                 val1 = mem[stack_pointer]
#                 val2 = mem[stack_pointer + 1]
#
#                 mem[stack_pointer + 1] = registers[Register.AC]
#                 registers[Register.SP] = (stack_pointer + 1) % DATA_SIZE
#
#                 log(f'{val1}, {val2} -> {registers[Register.AC]}', nest_level=1, do_log=do_log)
#         elif isinstance(instr, ArgInstruction):
#             instr_arg_type = instr.get_arg_value_type()
#             instr_arg = instr.get_arg_value()
#
#             if instr.opcode == Opcode.LD:
#                 if instr_arg_type == ValueType.IMMEDIATE:
#                     registers[Register.AC] = instr_arg
#
#                     log(f'IMM: {instr_arg}, AC: {registers[Register.AC]}', nest_level=1, do_log=do_log)
#                 elif instr_arg_type == ValueType.DIRECT:
#                     registers[Register.AC] = mem[instr_arg]
#
#                     log(f'MEM: {mem[instr_arg]}, AC: {registers[Register.AC]}', nest_level=1, do_log=do_log)
#                 elif instr_arg_type == ValueType.STACK:
#                     stack_pointer = registers[Register.SP]
#                     registers[Register.AC] = mem[stack_pointer + instr_arg]
#
#                     log(f'MEM: {mem[stack_pointer + instr_arg]}, AC: {registers[Register.AC]}', nest_level=1, do_log=do_log)
#             elif instr.opcode == Opcode.ST:
#                 if instr_arg_type == ValueType.DIRECT:
#                     mem[instr_arg] = registers[Register.AC]
#
#                     log(f'AC: {registers[Register.AC]}, MEM: {mem[instr_arg]}', nest_level=1, do_log=do_log)
#                 elif instr_arg_type == ValueType.STACK:
#                     stack_pointer = registers[Register.SP]
#                     mem[stack_pointer + instr_arg] = registers[Register.AC]
#
#                     log(f'AC: {registers[Register.AC]}, MEM: {mem[stack_pointer + instr_arg]}', nest_level=1, do_log=do_log)
#             elif instr.opcode == Opcode.ISP:
#                 stack_pointer = registers[Register.SP]
#                 log(f'SP: {stack_pointer}', nest_level=1, do_log=do_log)
#                 registers[Register.SP] = (stack_pointer + instr_arg) % DATA_SIZE
#
#                 log(f'SP: {stack_pointer} + {instr_arg} = {registers[Register.SP]}', nest_level=1, do_log=do_log)
#             elif instr.opcode == Opcode.DSP:
#                 stack_pointer = registers[Register.SP]
#                 log(f'SP: {stack_pointer}', nest_level=1, do_log=do_log)
#                 registers[Register.SP] = (stack_pointer - instr_arg) % DATA_SIZE
#
#                 log(f'SP: {stack_pointer} - {instr_arg} = {registers[Register.SP]}', nest_level=1, do_log=do_log)
#             elif instr.opcode == Opcode.JMP:
#                 if instr_arg_type == ValueType.DIRECT:
#                     registers[Register.IP] = instr_arg
#                 elif instr_arg_type == ValueType.INDIRECT:
#                     registers[Register.IP] += instr_arg
#                 elif instr_arg_type == ValueType.STACK:
#                     registers[Register.IP] = registers[Register.SP] + instr_arg
#             elif instr.opcode == Opcode.JZ:
#                 if registers[Register.AC] == 0:
#                     if instr_arg_type == ValueType.DIRECT:
#                         registers[Register.IP] = instr_arg
#                     elif instr_arg_type == ValueType.INDIRECT:
#                         registers[Register.IP] += instr_arg
#                     elif instr_arg_type == ValueType.STACK:
#                         registers[Register.IP] = registers[Register.SP] + instr_arg
#             elif instr.opcode == Opcode.JNZ:
#                 if registers[Register.AC] != 0:
#                     if instr_arg_type == ValueType.DIRECT:
#                         registers[Register.IP] = instr_arg
#                     elif instr_arg_type == ValueType.INDIRECT:
#                         registers[Register.IP] += instr_arg
#                     elif instr_arg_type == ValueType.STACK:
#                         registers[Register.IP] = registers[Register.SP] + instr_arg
#
#         log('', do_log=do_log)

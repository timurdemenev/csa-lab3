import sys
import lang.ast.ast_parser as ast_parser
import lang.expression_parser as expr_parser
import lang.lexer
from lang.assembly.asm_translator import translate
from model import machine


# print(expr_parser.parse_expr('a + b * c + d'))
# print(expr_parser.parse_expr('a ** b ** c'))
# print(expr_parser.parse_expr('a * (b + c) + x * y'))
# print(expr_parser.parse_expr('a + b * c ** d + e'))
# print(expr_parser.parse_expr('a + 5 > 0 & b + 9 == 4'))
# print(expr_parser.parse_expr('a > 0 & b > 5 | c > 3 & d'))
# print(expr_parser.parse_expr('a + 5 > 10 & b == 4 | c != 6 & d * 6 <= 216'))
# print(expr_parser.parse_expr('1 + 2 ** 2 ** 3 * 10 + 4'))
# print(expr_parser.parse_expr('1 + 10 + 2 ** 2 ** 3 + 4'))
# print(expr_parser.parse_expr('2 * 3 * 4 + 5'))
# print(expr_parser.parse_expr('5 - 2 + 3 * 4 * 5 / 6'))
# print(expr_parser.parse_expr('(a + 5) * b + c * d'))
# print(ast_parser.build_ast('val x = 1 + 2 + 3;\nval y = (x + 1) * 5;'))
# print(ast_parser.build_ast('while (x + 5) {\nval x = 5;\nv\n}'))
# print(ast_parser.read_var_decl('val x = 0;'))
# print(ast_parser.build_ast("""
#     main {
#         while (a + 5 > 10) {
#             val x = 5;
#             val y = 6;
#             y += (1 + 2) * 3;
#
#             for (val i = 0; i > 5; i += 1) {
#                 val z = 8;
#                 z += 3;
#             }
#
#             if (b > 0) {
#                 val z = 7;
#             }
#         }
#     }
# """))

# test_program = """
#     const limit = 4000000;
#
#     main {
#         val fib1 = 0;
#         val fib2 = 1;
#         val sum = 0;
#
#         while (fib2 < limit) {
#             const t = fib2;
#             fib2 += fib1;
#             fib1 = t;
#
#             if (fib2 % 2 == 0) {
#                 sum += fib2;
#             }
#         }
#     }
# """

# tok_line = "put '\n'"
# tokenizing_line = tok_line[:]

# while (token_tuple := lang.lexer.next_token(tokenizing_line)) is not None:
#     token, offset = token_tuple
#     print(token)
#     tokenizing_line = tokenizing_line[(offset + len(token)):]

# print(expr_parser.parse_expr("'0' + x"))

# program = open('main2.t').read()

# print(ast_parser.build_ast(program))

# exit(0)

args = len(sys.argv)

if args == 1:
    print('Usage: ./lab3.py <input-file>')

    exit(-1)

test_program = open(sys.argv[1]).read()

input_file = sys.argv[2] if args == 3 else None

ast_node = ast_parser.build_ast(test_program)
# print('Program AstNode:', ast_node, sep='\n')

instruction_list = translate(ast_node)

# for instr in instruction_list:
#     machine.print_instruction(instr)

# print('Executing on machine')
# machine.execute(instruction_list, memory_size=16, do_log=False)
# machine.execute(instruction_list, memory_size=32, input_file=input_file, do_log=False, dump=False)
# machine.execute(instruction_list, input_file=input_file, memory_size=32, do_log=False, dump=False)
# machine.execute(instruction_list, input_file=input_file, memory_size=32, do_log=False, dump=False)
machine.execute(instruction_list, input_file=input_file, do_log=False, dump=False)

const limit = 4000000;

main {
    val fib1 = 0;
    val fib2 = 1;
    val sum = 0;

    while (fib2 < limit) {
        const t = fib2;
        fib2 += fib1;
        fib1 = t;

        if (fib2 % 2 == 0) {
            sum += fib2;
        }
    }

    val x = 1;

    while (x * 10 < sum) {
        x *= 10;
    }

    puts "Sum: ";

    while (sum > 0) {
        const digit = sum / x;
        put '0' + digit;
        sum %= x;
        x /= 10;
    }

    put '\n';
}

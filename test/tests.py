from lang import expression_parser
from lang.expression_parser import *

expressions = [
    'a + b',
    'a + b * c',
    'a * b + c * d',
    'a * (b + c)',
    'a + b * c * d * e + f * g - h',
    'a + b * c * d * e / f + g * h - i'
]


def infix_to_postfix(infixexpr: str):
    """
    Converts infix expression to postfix expression
    Algorithm from https://runestone.academy/ns/books/published/pythonds/BasicDS/InfixPrefixandPostfixExpressions.html
    """
    prec = {
        "*": 3,
        "/": 3,
        "+": 2,
        "-": 2,
        "(": 1
    }
    opStack = []
    postfixList = []
    tokenList = infixexpr.replace('(', ' ( ').replace(')', ' ) ').split()

    for token in tokenList:
        if token in "abcdefghijklmnopqrstuvwxyz":
            postfixList.append(token)
        elif token == '(':
            opStack.append(token)
        elif token == ')':
            topToken = opStack.pop()
            while topToken != '(':
                postfixList.append(topToken)
                topToken = opStack.pop()
        else:
            while len(opStack) > 0 and (prec[opStack[-1]] >= prec[token]):
                  postfixList.append(opStack.pop())
            opStack.append(token)

    while len(opStack):
        postfixList.append(opStack.pop())
    return " ".join(postfixList)


def to_postfix(expr_node: ExpressionNode) -> str:
    if isinstance(expr_node, ValueNode):
        return expr_node.get_node_string()
    elif isinstance(expr_node, BinaryOperatorNode):
        return "{} {} {}".format(
                to_postfix(expr_node.get_left_node()),
                to_postfix(expr_node.get_right_node()),
                expr_node.get_node_string())


def expression_parse_test():
    for i, expr in enumerate(expressions):
        print(f'Test #{i + 1} -- {expr}')

        expr_node = expression_parser.parse_expr(expr)
        test_val = to_postfix(expr_node)
        true_val = infix_to_postfix(expr)

        if test_val != true_val:
            print(f'Test #{i + 1} failed!')
            print(f'Got {test_val}, expected {true_val}')

            exit(i + 1)
        else:
            print(f'Test #{i + 1} passed!')


expression_parse_test()

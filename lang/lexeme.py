from enum import Enum


class Lexeme(str, Enum):
    FOR_KEYWORD_LEXEME = 'for'
    VAL_KEYWORD_LEXEME = 'val'
    IF_KEYWORD_LEXEME = 'if'
    ELSE_KEYWORD_LEXEME = 'else'
    VOID_KEYWORD_LEXEME = 'void'
    WHILE_KEYWORD_LEXEME = 'while'
    DO_KEYWORD_LEXEME = 'do'
    CONTINUE_KEYWORD_LEXEME = 'continue'
    BREAK_KEYWORD_LEXEME = 'break'
    CONST_KEYWORD_LEXEME = 'const'
    RETURN_KEYWORD_LEXEME = 'return'
    TRUE_KEYWORD_LEXEME = 'true'
    FALSE_KEYWORD_LEXEME = 'false'
    MAIN_KEYWORD_LEXEME = 'main'
    ASSIGN_OPERATOR_LEXEME = '='
    COMMA_OPERATOR_LEXEME = ','
    PLUS_OPERATOR_LEXEME = '+'
    MINUS_OPERATOR_LEXEME = '-'
    MULTIPLY_OPERATOR_LEXEME = '*'
    DIVIDE_OPERATOR_LEXEME = '/'
    MODULO_OPERATOR_LEXEME = '%'
    POWER_OPERATOR_LEXEME = '**'
    AND_OPERATOR_LEXEME = '&'
    OR_OPERATOR_LEXEME = '|'
    INCREMENT_OPERATOR_LEXEME = '++'
    DECREMENT_OPERATOR_LEXEME = '--'
    NOT_OPERATOR_LEXEME = '!'
    PLUS_ASSIGN_OPERATOR_LEXEME = '+='
    MINUS_ASSIGN_OPERATOR_LEXEME = '-='
    MULTIPLY_ASSIGN_OPERATOR_LEXEME = '*='
    DIVIDE_ASSIGN_OPERATOR_LEXEME = '/='
    MODULO_ASSIGN_OPERATOR_LEXEME = '%='
    AND_ASSIGN_OPERATOR_LEXEME = '&='
    OR_ASSIGN_OPERATOR_LEXEME = '|='
    EQUALS_OPERATOR_LEXEME = '=='
    NOT_EQUALS_OPERATOR_LEXEME = '!='
    GREATER_OPERATOR_LEXEME = '>'
    GREATER_EQUAL_OPERATOR_LEXEME = '>='
    LESS_OPERATOR_LEXEME = '<'
    LESS_EQUAL_OPERATOR_LEXEME = '<='
    PUT_OPERATOR_LEXEME = 'put'
    PUTS_OPERATOR_LEXEME = 'puts'
    GET_OPERATOR_LEXEME = 'get'
    SEMICOLON_LEXEME = ';'
    LEFT_BRACE_LEXEME = '{'
    RIGHT_BRACE_LEXEME = '}'
    LEFT_BRACKET_LEXEME = '['
    RIGHT_BRACKET_LEXEME = ']'
    LEFT_PARENTHESIS_LEXEME = '('
    RIGHT_PARENTHESIS_LEXEME = ')'


lexemes_list = [e for e in Lexeme]


import lang.lexer as lex
from lang.lexeme import Lexeme
from lang.ast.ast import *
import lang.expression_parser as expr_parser

conditional_lexemes = [
    Lexeme.WHILE_KEYWORD_LEXEME,
    Lexeme.IF_KEYWORD_LEXEME
]

assign_lexemes = [
    Lexeme.ASSIGN_OPERATOR_LEXEME,
    Lexeme.PLUS_ASSIGN_OPERATOR_LEXEME,
    Lexeme.MINUS_ASSIGN_OPERATOR_LEXEME,
    Lexeme.MULTIPLY_ASSIGN_OPERATOR_LEXEME,
    Lexeme.DIVIDE_ASSIGN_OPERATOR_LEXEME,
    Lexeme.MODULO_ASSIGN_OPERATOR_LEXEME,
    Lexeme.AND_ASSIGN_OPERATOR_LEXEME,
    Lexeme.OR_ASSIGN_OPERATOR_LEXEME
]


def is_conditional(lexeme: str):
    return lexeme in conditional_lexemes


def is_assign(lexeme: str):
    return lexeme in assign_lexemes


def construction_ast_node_by_lexeme(lexeme: str) -> ConstructionASTNode:
    if lexeme == Lexeme.WHILE_KEYWORD_LEXEME:
        return WhileASTNode()
    elif lexeme == Lexeme.IF_KEYWORD_LEXEME:
        return IfASTNode()
    elif lexeme == Lexeme.FOR_KEYWORD_LEXEME:
        return ForASTNode()


def read_var_decl(text: str, bracket_level=0, return_offset=False):
    text_offset = 0
    current_node = None

    while (lex_tuple := lex.next_token(text[text_offset:])) is not None:
        lex_str, lex_offset = lex_tuple
        prev_text_offset = text_offset
        text_offset += lex_offset + len(lex_str)

        if lex_str == Lexeme.VAL_KEYWORD_LEXEME:
            current_node = VarDeclASTNode()
        elif lex_str == Lexeme.CONST_KEYWORD_LEXEME:
            current_node = ConstDeclAssignASTNode()
        elif isinstance(current_node, VarDeclASTNode) or isinstance(current_node, ConstDeclAssignASTNode):
            var_flag = isinstance(current_node, VarDeclASTNode)

            if lex.is_word(lex_str):
                current_node.set_var_name(lex_str)
            elif lex_str == Lexeme.ASSIGN_OPERATOR_LEXEME:
                decl_assign_node = VarDeclAssignASTNode(current_node.get_var_name()) if var_flag else current_node

                expr_node, expr_offset = expr_parser.parse_expr(text[text_offset:], bracket_level=bracket_level, return_offset=True)
                decl_assign_node.set_value_node(ExpressionASTNode(expr_node))
                text_offset += expr_offset
                current_node = decl_assign_node
            elif lex_str == Lexeme.SEMICOLON_LEXEME:
                return (current_node, prev_text_offset) if return_offset else current_node
        elif lex.is_word(lex_str):
            current_node = VarASTNode(lex_str)
        elif isinstance(current_node, ExpressionASTNode):
            if lex_str == Lexeme.ASSIGN_OPERATOR_LEXEME:
                assign_node = VarAssignASTNode(current_node.get_expression_node().get_node_string())
                current_node = assign_node
        elif isinstance(current_node, VarASTNode):
            if is_assign(lex_str):
                assign_node = VarAssignASTNode(var_name=current_node.get_var_name(), assign_operator=lex_str)

                expr_node, expr_offset = expr_parser.parse_expr(text[text_offset:], bracket_level=bracket_level, return_offset=True)
                assign_node.set_value_node(ExpressionASTNode(expr_node))
                text_offset += expr_offset
                current_node = assign_node

        elif isinstance(current_node, VarAssignASTNode):
            if lex_str == Lexeme.SEMICOLON_LEXEME or lex_str == Lexeme.RIGHT_PARENTHESIS_LEXEME and bracket_level > 0:
                return (current_node, prev_text_offset) if return_offset else current_node

    return None


def build_ast(text: str) -> ASTNode:
    text_offset = 0
    program_code_block_ast_node: CodeBlockASTNode = CodeBlockASTNode()
    code_block_nodes_stack = [program_code_block_ast_node]
    current_code_block = code_block_nodes_stack[-1]
    current_node = program_code_block_ast_node
    conditional_construction = False

    while (lex_tuple := lex.next_token(text[text_offset:])) is not None:
        lex_str, lex_offset = lex_tuple
        prev_text_offset = text_offset
        text_offset += lex_offset + len(lex_str)

        if is_conditional(lex_str) or lex_str == Lexeme.FOR_KEYWORD_LEXEME:
            node = construction_ast_node_by_lexeme(lex_str)
            code_block_nodes_stack[-1].add_ast_node_to_list(node)
            current_node = node
            code_block_nodes_stack.append(node.get_code_block_ast_node())
            conditional_construction = True
        elif lex_str == Lexeme.MAIN_KEYWORD_LEXEME:
            node = MainFunctionASTNode()
            code_block_nodes_stack[-1].add_ast_node_to_list(node)
            current_node = node
            code_block_nodes_stack.append(node.get_code_block_ast_node())
            conditional_construction = True
        else:
            if conditional_construction:
                if lex_str == Lexeme.LEFT_PARENTHESIS_LEXEME:
                    if isinstance(current_node, ConditionalASTNode):
                        expr_node, expr_offset = expr_parser.parse_expr(text[text_offset:], bracket_level=1, return_offset=True)
                        current_node.set_condition_ast_node(expr_node)
                        text_offset += expr_offset
                    elif isinstance(current_node, ForASTNode):
                        expr_node, expr_offset = read_var_decl(text[text_offset:], return_offset=True)
                        current_node.set_init_expression(expr_node)
                        text_offset += expr_offset + len(Lexeme.SEMICOLON_LEXEME)

                        expr_node, expr_offset = expr_parser.parse_expr(text[text_offset:], bracket_level=0, return_offset=True)
                        current_node.set_condition_expression(ExpressionASTNode(expr_node))
                        text_offset += expr_offset + len(Lexeme.SEMICOLON_LEXEME)

                        expr_node, expr_offset = read_var_decl(text[text_offset:], bracket_level=1, return_offset=True)
                        current_node.set_step_expression(expr_node)
                        text_offset += expr_offset + len(Lexeme.SEMICOLON_LEXEME)
                elif lex_str == Lexeme.LEFT_BRACE_LEXEME:
                    current_code_block = code_block_nodes_stack[-1]
                    current_node = current_code_block
                    conditional_construction = False
            else:
                if lex_str == Lexeme.LEFT_BRACE_LEXEME:
                    new_code_block_construction = ConstructionASTNode('code-block-construction')
                    code_block_nodes_stack[-1].add_ast_node_to_list(new_code_block_construction)
                    code_block_nodes_stack.append(new_code_block_construction.get_code_block_ast_node())
                    current_code_block = code_block_nodes_stack[-1]
                    current_node = current_code_block
                elif lex_str == Lexeme.RIGHT_BRACE_LEXEME:
                    code_block_nodes_stack.pop()
                    current_code_block = code_block_nodes_stack[-1]
                elif lex_str == Lexeme.SEMICOLON_LEXEME:
                    current_code_block.add_ast_node_to_list(current_node)
                    current_node = None
                elif lex_str == Lexeme.CONTINUE_KEYWORD_LEXEME:
                    current_node = ContinueActionASTNode()
                elif lex_str == Lexeme.BREAK_KEYWORD_LEXEME:
                    current_node = BreakActionASTNode()
                elif lex_str == Lexeme.PUT_OPERATOR_LEXEME:
                    current_node = PutOperatorASTNode()

                    put_arg_node, _offset = expr_parser.parse_expr(text[text_offset:], return_offset=True)

                    current_node.set_character(put_arg_node)

                    text_offset += _offset
                elif lex_str == Lexeme.PUTS_OPERATOR_LEXEME:
                    current_node = PutsOperatorASTNode()

                    puts_arg, _offset = lex.next_token(text[text_offset:])

                    assert lex.token_type(puts_arg) == 3

                    puts_arg_len = len(puts_arg)

                    for k, v in expr_parser.escape_symbols.items():
                        puts_arg = puts_arg.replace(k, v)

                    current_node.set_string_literal_node(StringLiteralASTNode(puts_arg))

                    text_offset += _offset + puts_arg_len
                elif lex_str == Lexeme.GET_OPERATOR_LEXEME:
                    current_node = GetOperatorASTNode()

                    get_arg, _offset = lex.next_token(text[text_offset:])

                    assert lex.token_type(get_arg) == 1

                    current_node.set_variable(get_arg)

                    text_offset += _offset + len(get_arg)
                else:
                    current_node, _offset = read_var_decl(text[prev_text_offset:], return_offset=True)
                    text_offset = prev_text_offset + _offset

    return program_code_block_ast_node

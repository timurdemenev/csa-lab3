
from enum import Enum
from lang.expression_parser import ExpressionNode


class ASTNodeNames(str, Enum):
    EXPRESSION = 'expression'
    STRING_LITERAL = 'string-literal'
    CODE_BLOCK = 'code-block'
    IF = 'if'
    WHILE = 'while'
    FOR = 'for'
    PUT_OPERATOR = 'put'
    PUTS_OPERATOR = 'puts'
    GET_OPERATOR = 'get'
    VAR_ASSIGN = 'var-assign'
    VAR_DECL = 'var-decl'
    VAR_DECL_ASSIGN = 'var-decl-assign'
    CONST_DECL_ASSIGN = 'const-decl-assign'
    CONTINUE = 'continue'
    BREAK = 'break'
    MAIN_FUNCTION = 'main-function'


class ASTNode:
    def __init__(self, node_name: str):
        self.node_name = node_name

    def get_node_name(self) -> str:
        return self.node_name

    def stringify(self) -> str:
        pass

    def __str__(self):
        return f"[{self.node_name}]{self.stringify()}"

    __repr__ = __str__


class ExpressionASTNode(ASTNode):
    def __init__(self, expression_node: ExpressionNode):
        super().__init__(node_name=ASTNodeNames.EXPRESSION)
        self.expression_node = expression_node

    def set_expression_node(self, expression_node: ExpressionNode):
        self.expression_node = expression_node

    def get_expression_node(self) -> ExpressionNode:
        return self.expression_node

    def stringify(self) -> str:
        return '\n' + str(self.expression_node)


class StringLiteralASTNode(ASTNode):
    def __init__(self, string_value=''):
        super().__init__(node_name=ASTNodeNames.STRING_LITERAL)
        self.string_value = string_value

    def set_string_value(self, string_value: str):
        self.string_value = string_value

    def get_string_value(self) -> str:
        return self.string_value

    def stringify(self) -> str:
        return '\nstring_value=' + self.string_value


class CodeBlockASTNode(ASTNode):
    def __init__(self, ast_nodes_list=None):
        super().__init__(node_name=ASTNodeNames.CODE_BLOCK)

        if ast_nodes_list is None:
            self.ast_nodes_list = []
        else:
            self.ast_nodes_list = ast_nodes_list[:]

    def add_ast_node_to_list(self, ast_node: ASTNode):
        self.ast_nodes_list.append(ast_node)

    def get_ast_node_list(self) -> list[ASTNode]:
        return self.ast_nodes_list

    def stringify(self) -> str:
        res_str = "{\n"

        for node in self.ast_nodes_list:
            res_str += f"\t{node}\n"

        return res_str + '}'


class ConstructionASTNode(ASTNode):
    def __init__(self, node_name: str, code_block=None):
        super().__init__(node_name)
        self.code_block = CodeBlockASTNode() if code_block is None else code_block

    def set_code_block_ast_node(self, code_block: CodeBlockASTNode):
        self.code_block = code_block

    def get_code_block_ast_node(self) -> CodeBlockASTNode:
        return self.code_block

    def stringify(self) -> str:
        return f"\ncode_block={self.code_block}"


class ConditionalASTNode(ConstructionASTNode):
    def __init__(self, node_name: str, condition_ast_node=None, code_block=None):
        super().__init__(node_name, code_block)
        self.condition_ast_node = condition_ast_node

    def set_condition_ast_node(self, condition_ast_node: ExpressionASTNode):
        self.condition_ast_node = condition_ast_node

    def get_condition_ast_node(self) -> ExpressionASTNode:
        return self.condition_ast_node

    def stringify(self) -> str:
        return f"( condition=\n{self.condition_ast_node},\ncode_block={self.code_block} )"


class IfASTNode(ConditionalASTNode):
    def __init__(self, condition_ast_node=None, code_block=None):
        super().__init__(node_name=ASTNodeNames.IF, condition_ast_node=condition_ast_node, code_block=code_block)


class WhileASTNode(ConditionalASTNode):
    def __init__(self, condition_ast_node=None, code_block=None):
        super().__init__(node_name=ASTNodeNames.WHILE, condition_ast_node=condition_ast_node, code_block=code_block)


class VarDeclASTNode(ASTNode):
    def __init__(self, node_name=None, var_name=None):
        super().__init__(node_name=ASTNodeNames.VAR_DECL if node_name is None else node_name)
        self.var_name = var_name

    def get_var_name(self) -> str:
        return self.var_name

    def set_var_name(self, var_name: str):
        self.var_name = var_name

    def stringify(self) -> str:
        return f"( var_name={self.var_name} )"


class ConstDeclAssignASTNode(ASTNode):
    def __init__(self, var_name=None, value_ast_node=None):
        super().__init__(node_name=ASTNodeNames.CONST_DECL_ASSIGN)
        self.var_name = var_name
        self.val_node = value_ast_node

    def get_value_node(self) -> ExpressionASTNode:
        return self.val_node

    def set_value_node(self, value_ast_node: ExpressionASTNode):
        self.val_node = value_ast_node

    def get_var_name(self) -> str:
        return self.var_name

    def set_var_name(self, var_name: str):
        self.var_name = var_name

    def stringify(self) -> str:
        return f"( var_name={self.var_name}, val_node=\n{self.val_node} )"


class VarDeclAssignASTNode(VarDeclASTNode):
    def __init__(self, var_name=None, value_ast_node=None):
        super().__init__(node_name=ASTNodeNames.VAR_DECL_ASSIGN, var_name=var_name)
        self.val_node = value_ast_node

    def get_value_node(self) -> ExpressionASTNode:
        return self.val_node

    def set_value_node(self, value_ast_node: ExpressionASTNode):
        self.val_node = value_ast_node

    def stringify(self) -> str:
        return f"( var_name={self.var_name}, val_node=\n{self.val_node} )"


class VarAssignASTNode(ASTNode):
    def __init__(self, var_name=None, value_node=None, assign_operator=None):
        super().__init__(ASTNodeNames.VAR_ASSIGN)
        self.var_name = var_name
        self.value_node = value_node
        self.assign_operator = assign_operator

    def get_var_name(self) -> str:
        return self.var_name

    def set_var_name(self, var_name: str):
        self.var_name = var_name

    def get_value_node(self) -> ExpressionASTNode:
        return self.value_node

    def set_value_node(self, value_node: ExpressionASTNode):
        self.value_node = value_node

    def get_assign_operator(self) -> str:
        return self.assign_operator

    def set_assign_operator(self, assign_operator: str):
        self.assign_operator = assign_operator

    def stringify(self) -> str:
        return f"( var_name={self.var_name}, assign_operator='{self.assign_operator}', value_node={self.value_node} )"


class VarASTNode(ASTNode):
    def __init__(self, var_name: str):
        super().__init__('var')
        self.var_name = var_name

    def get_var_name(self) -> str:
        return self.var_name


class PutOperatorASTNode(ASTNode):
    def __init__(self, character_node=None):
        super().__init__(node_name=ASTNodeNames.PUT_OPERATOR)
        self.character_node = character_node

    def set_character(self, character_node: ExpressionASTNode):
        self.character_node = character_node

    def get_character(self) -> ExpressionASTNode:
        return self.character_node

    def stringify(self) -> str:
        return f'( character={self.character_node} )'


class PutsOperatorASTNode(ASTNode):
    def __init__(self, string_node=None):
        super().__init__(node_name=ASTNodeNames.PUTS_OPERATOR)
        self.string_node = string_node

    def set_string_literal_node(self, string_node: StringLiteralASTNode):
        self.string_node = string_node

    def get_string_literal_node(self) -> StringLiteralASTNode:
        return self.string_node

    def stringify(self) -> str:
        return f'( string_node={self.string_node} )'


class GetOperatorASTNode(ASTNode):
    def __init__(self, variable=None):
        super().__init__(node_name=ASTNodeNames.GET_OPERATOR)
        self.variable = variable

    def set_variable(self, variable: str):
        self.variable = variable

    def get_variable(self) -> str:
        return self.variable

    def stringify(self) -> str:
        return f'( var={self.variable} )'

# class FunctionCallASTNode(ExpressionASTNode):
#     def __init__(self, function_name=None, args_list=None):
#         self.node_name = 'function-call'
#         self.function_name = function_name
#         self.args_list = [] if args_list is None else args_list
#
#     def set_function_name(self, function_name: str):
#         self.function_name = function_name
#
#     def get_function_name(self) -> str:
#         return self.function_name
#
#     def add_arg(self, expression_ast_node: ExpressionASTNode):
#         self.args_list.append(expression_ast_node)
#
#     def stringify(self) -> str:
#         str_res = self.function_name + "( "
#
#         if len(self.args_list) > 0:
#             for i, arg in range(len(self.args_list) - 1):
#                 str_res += str(arg) + ", "
#
#             str_res += str(self.args_list[-1])
#
#         str_res += " )"
#
#         return str_res


class ForASTNode(ConstructionASTNode):
    def __init__(self, init_expression=None, condition_expression=None, step_expression=None, code_block=None):
        super().__init__(node_name=ASTNodeNames.FOR, code_block=code_block)
        self.init_expression = init_expression
        self.condition_expression = condition_expression
        self.step_expression = step_expression

    def set_init_expression(self, init_expression: ExpressionASTNode):
        self.init_expression = init_expression

    def get_init_expression(self) -> ExpressionASTNode:
        return self.init_expression

    def set_condition_expression(self, condition_expression: ExpressionASTNode):
        self.condition_expression = condition_expression

    def get_condition_expression(self) -> ExpressionASTNode:
        return self.condition_expression

    def set_step_expression(self, step_expression: ExpressionASTNode):
        self.step_expression = step_expression

    def get_step_expression(self) -> ExpressionASTNode:
        return self.step_expression

    def stringify(self) -> str:
        return f"( init={self.init_expression}, condition={self.condition_expression}, step={self.step_expression}, code_block={self.code_block} )"


class ContinueActionASTNode(ASTNode):
    def __init__(self):
        super().__init__(node_name=ASTNodeNames.CONTINUE)

    def stringify(self) -> str:
        return ""


class BreakActionASTNode(ASTNode):
    def __init__(self):
        super().__init__(node_name=ASTNodeNames.BREAK)

    def stringify(self) -> str:
        return ""


class MainFunctionASTNode(ConstructionASTNode):
    def __init__(self, code_block=None):
        super().__init__(node_name=ASTNodeNames.MAIN_FUNCTION, code_block=code_block)


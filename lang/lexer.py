from lang.lexeme import lexemes_list
from typing import Optional


class Token:
    def __init__(self, line: int, offset: int, token: str):
        self.line = line
        self.offset = offset
        self.token = token[:]
        self.token_len = len(self.token)

    def get_line(self):
        return self.line

    def get_offset(self):
        return self.offset

    def get_token_string(self):
        return self.token

    def get_token_length(self):
        return self.token_len

    def __str__(self):
        return f"Token[line={self.line}, offset={self.offset}, token=\"{self.token}\", token_type={token_type(self.token)}]"

    __repr__ = __str__


def is_string_literal(token: str) -> bool:  # finish it
    if len(token) < 2:
        return False

    if token[0] == '"' and token[-1] == '"':
        return True

    return False


def is_char(token: str) -> bool:
    if len(token) < 2:
        return False

    if token[0] == '\'' and token[-1] == '\'':
        backslash_flag = False
        char_len = 0

        for c in token[1:-1]:
            if c == '\\':
                if not backslash_flag:
                    backslash_flag = True
                else:
                    char_len += 1
                    backslash_flag = False
            else:
                char_len += 1
                backslash_flag = False

            if char_len > 1:
                return False

        return True

    return False


def is_word(token: str) -> bool:
    return token.isidentifier()


def is_num(token: str) -> bool:
    if token.startswith('0x') and len(token) > 2:
        return all(list(map(lambda x: '0' <= x <= '9' or 'a' <= x <= 'f', token[2:].lower())))
    elif token.startswith('0b') and len(token) > 2:
        return all(map(lambda x: '0' <= x <= '1', token[2:]))
    elif token.startswith('0') and len(token) > 1:
        return all(map(lambda x: '0' <= x <= '7', token[1:]))
    else:
        return token.isnumeric()


def token_type(token: str) -> int:
    if token in lexemes_list:
        return lexemes_list.index(token) + 4
    elif is_num(token):
        return 0
    elif is_word(token):
        return 1
    elif is_char(token):
        return 2
    elif is_string_literal(token):
        return 3
    else:
        return -1


def next_token(line: str) -> Optional[tuple[str, int]]:
    token_string: str = ''
    offset: int = 0
    single_quote_flag = False
    double_quote_flag = False
    backslash_flag = False

    for symb in line:
        if symb.isspace() and not (single_quote_flag or double_quote_flag):
            if len(token_string) > 0:
                return token_string, offset - len(token_string)
        else:
            prev_token_string: str = token_string
            token_string += symb

            if token_type(token_string) == -1 and token_type(prev_token_string) != -1 and \
                    not (token_string == '0x' or token_string == '0b'):
                return prev_token_string, offset - len(prev_token_string)

            if backslash_flag:
                backslash_flag = False
            else:
                if symb == '"' and not single_quote_flag:
                    double_quote_flag = not double_quote_flag
                elif symb == '\'' and not double_quote_flag:
                    single_quote_flag = not single_quote_flag
                elif symb == '\\':
                    backslash_flag = True

        offset += 1

    return (token_string, offset - len(token_string)) if token_type(token_string) != -1 else None


def tokenize(line: str, include_offset=False):
    found_tokens = []
    line_offset = 0
    tok_line = line[:]

    while (token_tuple := next_token(tok_line)) is not None:
        token_str, token_offset = token_tuple
        line_offset += token_offset

        if include_offset:
            found_tokens.append((token_str, line_offset))
        else:
            found_tokens.append(token_str)

        line_offset += len(token_str)

        tok_line = tok_line[token_offset + len(token_str):]

    return found_tokens


def parse(file: str) -> list:
    token_list = []

    with open(file, 'r') as f:
        for line_num, line in enumerate(f.readlines()):
            trimmed_line = line.strip()
            if len(trimmed_line) > 0:
                tok_list = tokenize(line, include_offset=True)

                for token_tuple in tok_list:
                    token_list.append(Token(line_num, *token_tuple[::-1]))

    return token_list


from enum import Enum


class Opcode(str, Enum):
#   LD X (load) - load X to ACC
    LD = 'ld'

#   ST X (save to) - save ACC to X
    ST = 'st'


#   NEG - negate ACC ACC = -ACC
    NEG = 'neg'


#   ADD X - add X to ACC; ACC = ACC + X
    ADD = 'add'

#   SUB X (subtract) - subtract X from ACC; ACC = ACC - X
    SUB = 'sub'

#   MUL X (multiply) - multiply ACC by X; ACC = ACC * X
    MUL = 'mul'

#   DIV X (divide) - divide ACC by X; ACC = ACC / X
    DIV = 'div'

#   MOD X (modulo) - divide ACC by X and save remainder to ACC; ACC = ACC % X
    MOD = 'mod'

#   XOR X - bitwise 'xor' between ACC and X; ACC = ACC ^ X
    XOR = 'xor'

#   AND X - bitwise 'and' between ACC and X; ACC = ACC & X
    AND = 'and'

#   OR X - bitwise 'or' between ACC and X; ACC = ACC | X
    OR = 'or'


#   JMP [ADDR] - jump to ADDR
    JMP = 'jmp'


#   PUSH - decrement SP (stack pointer) and write ACC to stack
    PUSH = 'push'

#   POP - read ACC from stack and increment sp (stack pointer)
    POP = 'pop'


#   ISP X (increment stack pointer) - add X to SP; SP = SP + X
    ISP = 'isp'

#   DSP X (decrement stack pointer) - subtract X from SP; SP = SP - X
    DSP = 'dsp'


#   ADDST (add from stack) - write to ACC sum of two values from the top of stack
    ADDST = 'addst'

#   SUBST (subtract from stack) - write to ACC difference of two values from the top of stack
    SUBST = 'subst'

#   MULST (multiply from stack) - write to ACC multiplication of two values from the top of stack
    MULST = 'mulst'

#   DIVST (division from stack) - write to ACC division of two values from the top of stack
    DIVST = 'divst'

#   MODST (modulo from stack) - write to ACC remainder of division for two values from the top of stack
    MODST = 'modst'

#   XORST (xor from stack) - write to ACC result of bitwise xor for two values from the top of stack
    XORST = 'xorst'

#   ANDST (and from stack) - write to ACC result of bitwise and for two values from the top of stack
    ANDST = 'andst'

#   ORST (or from stack) - write to ACC result of bitwise or for two values from the top of stack
    ORST = 'orst'


#   SETG X (set if greater from stack) - set ACC to 1 if first value from stack is greater than second one, otherwise ACC will be set to zero
    SETGST = 'setgst'

#   SETGE X (set if greater or equal from stack) - set ACC to 1 if first value from stack is greater or equal than second one, otherwise ACC will be set to zero
    SETGEST = 'setgest'

#   SETL X (set if less from stack) - set ACC to 1 if first value from stack is less than second one, otherwise ACC will be set to zero
    SETLST = 'setlst'

#   SETLE X (set if less or equal from stack) - set ACC to 1 if first value from stack is less or equal than second one, otherwise ACC will be set to zero
    SETLEST = 'setlest'

#   SETZ X (set if zero/equal from stack) - set ACC to 1 if two values from stack are equal, otherwise ACC will be set to zero
    SETZST = 'setzst'

#   SETNZ X (set if not zero/equal from stack) - set ACC to 1 if two values from stack are not equal, otherwise ACC will be set to zero
    SETNZST = 'setnzst'


#   JZ [ADDR] (jump if zero) - jump to ADDR if ACC is equal to zero
    JZ = 'jz'

#   JNZ [ADDR] (jump if not zero) - jump to ADDR if ACC is not equal to zero
    JNZ = 'jnz'

#   PUT - save character code to Output memory segment
    PUT = 'put'

#   GET - take character code from Input memory segment
    GET = 'get'


class StubOpcode(str, Enum):
    CONTINUE = 'continue'
    BREAK = 'break'


class ValueType(str, Enum):
    IMMEDIATE = 'imm'
    DIRECT = 'dir'
    INDIRECT = 'ind'
    STACK = 'stack'


class Instruction:
    def jsonify(self):
        pass

    def __str__(self):
        return str(self.jsonify())

    __repr__ = __str__


class OpcodeInstruction(Instruction):
    def __init__(self, opcode: Opcode or str):
        self.opcode = opcode if isinstance(opcode, Opcode) else Opcode[opcode]

    def get_opcode(self) -> Opcode:
        return self.opcode


class ArgInstruction(OpcodeInstruction):
    def __init__(self, opcode: Opcode or str, arg_val_type: ValueType, arg_value: int):
        super().__init__(opcode)
        self.arg_value_type = arg_val_type
        self.arg_value = arg_value

    def get_arg_value_type(self) -> ValueType:
        return self.arg_value_type

    def get_arg_value(self) -> int:
        return self.arg_value

    def jsonify(self):
        return {
            'opcode': self.opcode,
            'arg_value_type': self.arg_value_type,
            'arg_value': self.arg_value
        }


class NonArgInstruction(OpcodeInstruction):
    def jsonify(self):
        return {'opcode': self.opcode}


class StubInstruction(Instruction):
    def __init__(self, stub_opcode: StubOpcode or str):
        self.stub_opcode = stub_opcode if isinstance(stub_opcode, StubOpcode) else StubOpcode[stub_opcode]

    def get_stub_opcode(self):
        return self.stub_opcode

    def jsonify(self):
        return {'stub_opcode': self.stub_opcode}


def print_instruction(instr):
    if isinstance(instr, ArgInstruction):
        print(instr.get_opcode().value, ' ', sep='', end='')

        prefix = ''

        if instr.get_arg_value_type() == ValueType.STACK:
            prefix = '&'
        elif instr.get_arg_value_type() == ValueType.IMMEDIATE:
            prefix = '#'
        elif instr.get_arg_value_type() == ValueType.DIRECT:
            prefix = '$'

        print(f"{prefix}{instr.get_arg_value()}")
    elif isinstance(instr, NonArgInstruction):
        print(instr.get_opcode().value)

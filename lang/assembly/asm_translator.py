
from lang.ast.ast import *
from lang.assembly.isa import *
from lang.expression_parser import ExpressionNode, ValueNode, BinaryOperatorNode

import copy
from lang.lexeme import Lexeme


class VarType(str, Enum):
    GLOBAL = 'global'
    LOCAL = 'local'


class VarContext:
    def __init__(self, var_type: str or VarType, var_name: str, addr: int):
        self.var_type = var_type if isinstance(var_type, VarType) else VarType[var_type]
        self.var_name = var_name
        self.addr = addr

    def get_var_type(self) -> VarType:
        return self.var_type

    def get_var_name(self) -> str:
        return self.var_name

    def get_addr(self) -> int:
        return self.addr

    def __str__(self):
        return str({
            'var-type': self.var_type,
            'var-name': self.var_name,
            'addr': self.addr
        })

    __repr__ = __str__


class Context:
    def __init__(self):
        self.variables_context = dict()
        self.constants_context = dict()
        self.io_counter = 0
        self.instruction_counter = 0
        self.stack_base = 0
        self.stack_pointer = 0

    def add_var_to_context(self, var_context: VarContext):
        self.variables_context[var_context.get_var_name()] = var_context

    def get_variable_context(self) -> dict[str, VarContext]:
        return self.variables_context

    def add_const_to_context(self, const_context: VarContext):
        self.constants_context[const_context.get_var_name()] = const_context

    def get_constants_context(self) -> dict[str, VarContext]:
        return self.constants_context

    def inc_instruction_counter(self, x=1):
        self.instruction_counter += x

    def get_instruction_counter(self) -> int:
        return self.instruction_counter

    def dec_stack_pointer(self, x=1):
        self.stack_pointer -= x

    def inc_stack_pointer(self, x=1):
        self.stack_pointer += x

    def get_stack_pointer(self) -> int:
        return self.stack_pointer

    def set_stack_base(self, stack_base: int):
        self.stack_base = stack_base

    def get_stack_base(self) -> int:
        return self.stack_base


opcode_by_operator = {
    Lexeme.PLUS_OPERATOR_LEXEME.value: Opcode.ADDST,
    Lexeme.MINUS_OPERATOR_LEXEME.value: Opcode.SUBST,
    Lexeme.MULTIPLY_OPERATOR_LEXEME.value: Opcode.MULST,
    Lexeme.DIVIDE_OPERATOR_LEXEME.value: Opcode.DIVST,
    Lexeme.MODULO_OPERATOR_LEXEME.value: Opcode.MODST,

    Lexeme.AND_OPERATOR_LEXEME.value: Opcode.ANDST,
    Lexeme.OR_OPERATOR_LEXEME.value: Opcode.ORST,

    Lexeme.EQUALS_OPERATOR_LEXEME.value: Opcode.SETZST,
    Lexeme.NOT_EQUALS_OPERATOR_LEXEME.value: Opcode.SETNZST,
    Lexeme.GREATER_OPERATOR_LEXEME.value: Opcode.SETGST,
    Lexeme.GREATER_EQUAL_OPERATOR_LEXEME.value: Opcode.SETGEST,
    Lexeme.LESS_OPERATOR_LEXEME.value: Opcode.SETLST,
    Lexeme.LESS_EQUAL_OPERATOR_LEXEME.value: Opcode.SETLEST,
}

bin_op_from_assign_oper = {
    Lexeme.PLUS_ASSIGN_OPERATOR_LEXEME.value: Lexeme.PLUS_OPERATOR_LEXEME,
    Lexeme.MINUS_ASSIGN_OPERATOR_LEXEME.value: Lexeme.MINUS_OPERATOR_LEXEME,
    Lexeme.MULTIPLY_ASSIGN_OPERATOR_LEXEME.value: Lexeme.MULTIPLY_OPERATOR_LEXEME,
    Lexeme.DIVIDE_ASSIGN_OPERATOR_LEXEME.value: Lexeme.DIVIDE_OPERATOR_LEXEME,
    Lexeme.MODULO_ASSIGN_OPERATOR_LEXEME.value: Lexeme.MODULO_OPERATOR_LEXEME
}


def calc_stack_addr_for_var(var_name: str, context: Context):
    base_offset = context.get_stack_base() - context.get_stack_pointer()

    if context.get_variable_context().get(var_name) is not None:
        return base_offset - context.get_variable_context()[var_name].get_addr()
    elif context.get_constants_context().get(var_name) is not None:
        return base_offset - context.get_constants_context()[var_name].get_addr()
    else:
        return None


def translate_expression_node(expression_node: ExpressionASTNode or ExpressionNode, context: Context) -> list[Instruction]:
    if isinstance(expression_node, ExpressionASTNode):
        expression_node = expression_node.get_expression_node()

    instr_list: list[Instruction] = []

    if isinstance(expression_node, ValueNode):
        val_str = expression_node.get_node_string()

        if context.get_variable_context().get(val_str) is None and context.get_constants_context().get(val_str) is None:
            instr_list.append(ArgInstruction(Opcode.LD, ValueType.IMMEDIATE, int(val_str)))
        else:
            if context.get_variable_context().get(val_str):
                var_context = context.get_variable_context().get(val_str)
            else:
                var_context = context.get_constants_context().get(val_str)

            if var_context.get_var_type() == VarType.GLOBAL:
                instr_list.append(ArgInstruction(Opcode.LD, ValueType.DIRECT, var_context.get_addr()))
            elif var_context.get_var_type() == VarType.LOCAL:
                instr_list.append(ArgInstruction(Opcode.LD, ValueType.STACK, calc_stack_addr_for_var(var_context.get_var_name(), context)))

        instr_list.append(NonArgInstruction(Opcode.PUSH))
        context.dec_stack_pointer()
    elif isinstance(expression_node, BinaryOperatorNode):
        instr_list.extend(translate_expression_node(expression_node.get_right_node(), context))
        instr_list.extend(translate_expression_node(expression_node.get_left_node(), context))

        instr_list.append(NonArgInstruction(opcode_by_operator[expression_node.get_node_string()]))
        context.inc_stack_pointer()

    return instr_list


def process_var_decl_assign_ast_node(node: VarDeclAssignASTNode, stack_base: int, context: Context) -> list[Instruction]:
    instr_list = translate_expression_node(node.get_value_node(), context)
    context.add_var_to_context(VarContext(VarType.LOCAL, node.get_var_name(), stack_base - context.get_stack_pointer()))

    return instr_list


def process_const_decl_assign_ast_node(node: ConstDeclAssignASTNode, stack_base: int, context: Context) -> list[Instruction]:
    instr_list = translate_expression_node(node.get_value_node(), context)
    context.add_const_to_context(VarContext(VarType.LOCAL, node.get_var_name(), stack_base - context.get_stack_pointer()))

    return instr_list


def process_var_decl_ast_node(node: VarDeclASTNode, stack_base: int, context: Context) -> list[Instruction]:
    context.add_var_to_context(VarContext(VarType.LOCAL, node.get_var_name(), stack_base - context.get_stack_pointer()))
    context.dec_stack_pointer()

    return [
        ArgInstruction(Opcode.LD, ValueType.IMMEDIATE, 0),
        NonArgInstruction(Opcode.PUSH)
    ]


def process_var_assign_ast_node(node: VarAssignASTNode, stack_base: int, context: Context) -> list[Instruction]:
    instr_list = []

    assign_operator = node.get_assign_operator()

    if assign_operator != '=':
        value_node = BinaryOperatorNode(bin_op_from_assign_oper[assign_operator])
        value_node.set_left_node(ValueNode(node.get_var_name()))
        value_node.set_right_node(node.get_value_node())
    else:
        value_node = node.get_value_node()

    instr_list.extend(translate_expression_node(value_node, context))

    instr_list.append(NonArgInstruction(Opcode.POP))
    context.inc_stack_pointer()

    instr_list.append(
        ArgInstruction(Opcode.ST, ValueType.STACK, calc_stack_addr_for_var(node.get_var_name(), context)))

    return instr_list


def process_put_operator_ast_node(node: PutOperatorASTNode, stack_base: int, context: Context) -> list[Instruction]:
    instr_list = []

    character_instructions_list = translate_expression_node(node.get_character(), context)
    instr_list.extend(character_instructions_list)

    instr_list.extend([
        NonArgInstruction(Opcode.POP),
        NonArgInstruction(Opcode.PUT)
    ])

    context.inc_stack_pointer()

    return instr_list


def process_puts_operator_ast_node(node: PutsOperatorASTNode, stack_base: int, context: Context) -> list[Instruction]:
    instr_list = []

    puts_string = node.get_string_literal_node().get_string_value()

    for symbol in puts_string[1:-1]:
        instr_list.append(ArgInstruction(Opcode.LD, ValueType.IMMEDIATE, ord(symbol)))
        instr_list.append(NonArgInstruction(Opcode.PUT))

    return instr_list


def process_get_operator_ast_node(node: GetOperatorASTNode, stack_base: int, context: Context) -> list[Instruction]:
    get_variable = node.get_variable()

    return [
        NonArgInstruction(Opcode.GET),
        ArgInstruction(Opcode.ST, ValueType.STACK, calc_stack_addr_for_var(get_variable, context))
    ]


def process_while_ast_node(node: WhileASTNode, stack_base: int, context: Context) -> list[Instruction]:
    while_instr_list = translate_while(node, context)
    context.inc_instruction_counter(len(while_instr_list))

    return while_instr_list


def process_if_ast_node(node: IfASTNode, stack_base: int, context: Context) -> list[Instruction]:
    if_instr_list = translate_if(node, context)
    context.inc_instruction_counter(len(if_instr_list))

    return if_instr_list


def translate_construction_ast_node(construction_ast_node: ConstructionASTNode, context: Context) -> list[Instruction]:
    allocation_count = 0
    instr_list: list[Instruction] = []

    stack_base = context.get_stack_base()

    allocation_nodes = [
        ASTNodeNames.VAR_DECL,
        ASTNodeNames.VAR_DECL_ASSIGN,
        ASTNodeNames.CONST_DECL_ASSIGN
    ]

    processors_by_ast_node = {
        ASTNodeNames.VAR_DECL.value: process_var_decl_ast_node,
        ASTNodeNames.VAR_DECL_ASSIGN.value: process_var_decl_assign_ast_node,
        ASTNodeNames.CONST_DECL_ASSIGN.value: process_const_decl_assign_ast_node,
        ASTNodeNames.VAR_ASSIGN.value: process_var_assign_ast_node,
        ASTNodeNames.PUT_OPERATOR.value: process_put_operator_ast_node,
        ASTNodeNames.PUTS_OPERATOR.value: process_puts_operator_ast_node,
        ASTNodeNames.GET_OPERATOR.value: process_get_operator_ast_node,
        ASTNodeNames.WHILE.value: process_while_ast_node,
        ASTNodeNames.IF.value: process_if_ast_node
    }

    for node in construction_ast_node.get_code_block_ast_node().get_ast_node_list():
        node_name = node.get_node_name()

        if node_name in allocation_nodes:
            allocation_count += 1

        if node_name in processors_by_ast_node.keys():
            instr_list.extend(processors_by_ast_node[node_name](node, stack_base, context))
        elif node_name == ASTNodeNames.CONTINUE:
            instr_list.append(StubInstruction(StubOpcode.CONTINUE))
        elif node_name == ASTNodeNames.BREAK:
            instr_list.append(StubInstruction(StubOpcode.BREAK))
        elif isinstance(node, ConstructionASTNode):
            code_block_instr_list = translate_construction_ast_node(node, context)
            context.inc_instruction_counter(len(code_block_instr_list))
            instr_list.extend(code_block_instr_list)

    if allocation_count > 0:
        instr_list.append(ArgInstruction(Opcode.ISP, ValueType.IMMEDIATE, allocation_count))
        context.inc_stack_pointer(allocation_count)

    return instr_list


def translate_while(while_ast_node: WhileASTNode, context: Context) -> list[Instruction]:
    instr_list: list[Instruction] = []

    while_offset = 0

    while_condition_instructions = translate_expression_node(while_ast_node.get_condition_ast_node(), context)
    instr_list.extend(while_condition_instructions)
    instr_list.append(NonArgInstruction(Opcode.POP))
    context.inc_stack_pointer()

    while_offset += len(while_condition_instructions) + 1

    while_body_instr_list = translate_construction_ast_node(while_ast_node, context)

    while_offset += len(while_body_instr_list) + 1

    instr_list.append(ArgInstruction(Opcode.JZ, ValueType.INDIRECT, len(while_body_instr_list) + 1))
    instr_list.extend(while_body_instr_list)

    # while_offset - instr_num - len(while_condition_instructions)
    # len(while_condition_instructions) + len(while_body_instr_list) + 1 - instr_num - len(while_condition_instructions)
    # len(while_body_instr_list) + 1 - instr_num

    instr_list.append(ArgInstruction(Opcode.JMP, ValueType.INDIRECT, -(while_offset + 1)))

    return instr_list


def translate_if(if_ast_node: IfASTNode, context: Context) -> list[Instruction]:
    instr_list: list[Instruction] = []

    instr_list.extend(translate_expression_node(if_ast_node.get_condition_ast_node(), context))
    instr_list.append(NonArgInstruction(Opcode.POP))
    context.inc_stack_pointer()

    if_body_instr_list = translate_construction_ast_node(if_ast_node, context)

    instr_list.append(ArgInstruction(Opcode.JZ, ValueType.INDIRECT, len(if_body_instr_list)))
    instr_list.extend(if_body_instr_list)

    return instr_list


def translate(ast_node: ASTNode, context=None) -> list[Instruction]:
    if context is None:
        context = Context()

    data_memory_counter = 0
    instruction_list: list[Instruction] = []

    if isinstance(ast_node, CodeBlockASTNode):
        for node in ast_node.get_ast_node_list():
            if isinstance(node, ConstDeclAssignASTNode) or isinstance(node, VarDeclAssignASTNode):
                instruction_list.extend(translate_expression_node(node.get_value_node(), context))

                instruction_list.append(NonArgInstruction(Opcode.POP))
                context.inc_stack_pointer()

                instruction_list.append(ArgInstruction(Opcode.ST, ValueType.DIRECT, data_memory_counter))
                new_var_context = VarContext(VarType.GLOBAL, node.get_var_name(), data_memory_counter)

                if isinstance(node, ConstDeclAssignASTNode):
                    context.add_const_to_context(new_var_context)
                else:
                    context.add_var_to_context(new_var_context)

                data_memory_counter += 1
            elif isinstance(node, VarDeclASTNode):
                instruction_list.extend([
                    ArgInstruction(Opcode.LD, ValueType.IMMEDIATE, 0),
                    ArgInstruction(Opcode.ST, ValueType.DIRECT, data_memory_counter)
                ])
                context.add_var_to_context(VarContext(VarType.GLOBAL, node.get_var_name(), data_memory_counter))
                data_memory_counter += 1
            elif isinstance(node, MainFunctionASTNode):
                stack_base = context.get_stack_base()
                context.set_stack_base(context.get_stack_pointer())

                instr_lst = translate_construction_ast_node(node, copy.deepcopy(context))
                context.inc_instruction_counter(len(instr_lst))

                instruction_list.extend(instr_lst)

                context.set_stack_base(stack_base)

            context.inc_instruction_counter()

    return instruction_list


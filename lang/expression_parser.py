
from lang.lexer import next_token, tokenize, is_char, is_num
from lang.lexeme import Lexeme

operators_types = {
    '+': 'open',
    '-': 'open',
    '*': 'open',
    '/': 'open',
    '%': 'open',
    '**': 'open',

    '==': 'closed',
    '!=': 'closed',
    '>': 'closed',
    '>=': 'closed',
    '<': 'closed',
    '<=': 'closed',

    '&': 'open',
    '|': 'open'
}

operators_prior = {
    '+': 1,
    '-': 1,

    '*': 2,
    '/': 2,
    '%': 2,

    '**': 3,

    '==': -1,
    '!=': -1,
    '>': -1,
    '>=': -1,
    '<': -1,
    '<=': -1,

    '&': -2,
    '|': -3
}

escape_symbols = {
    '\\n': '\n',
    '\\b': '\b',
    '\\r': '\r',
    '\\0': '\0'
}


class ExpressionNode:
    def __init__(self, expr: str):
        self.node_str = expr
        self.parent_node = None

    def get_node_string(self):
        return self.node_str

    def get_parent_node(self):
        return self.parent_node

    def __str__(self):
        pass

    __repr__ = __str__


class ValueNode(ExpressionNode):
    def __init__(self, expr: str):
        super().__init__(expr)

    def __str__(self):
        return f"[{self.node_str}]"


class UnaryOperatorNode(ExpressionNode):
    def __init__(self, expr: str):
        super().__init__(expr)
        self.str = str
        self.expr_node = None

    def set_expression_node(self, expr_node: ExpressionNode):
        self.expr_node = expr_node

    def get_expression_node(self) -> ExpressionNode:
        return self.expr_node

    def stringify(self, tab_level=0):
        str_result = self.node_str + ' ->'

        if self.expr_node is not None:
            str_result += '\n' + '\t' * (tab_level + 1)
            if isinstance(self.expr_node, BinaryOperatorNode) or isinstance(self.expr_node, UnaryOperatorNode):
                str_result += self.expr_node.stringify(tab_level + 1)
            else:
                str_result += f"[{self.expr_node.node_str}]"

        return str_result

    def __str__(self):
        return self.stringify()


class BinaryOperatorNode(ExpressionNode):
    def __init__(self, expr: str):
        super().__init__(expr)
        self.op_prior = operators_prior[expr] if expr in operators_prior.keys() else 0
        self.left_node = None
        self.right_node = None

    def get_operator_prior(self):
        return self.op_prior

    def stringify(self, tab_level=0):
        str_result = self.node_str + ' ->'

        if self.left_node is not None:
            str_result += '\n' + '\t' * (tab_level + 1)
            if isinstance(self.left_node, BinaryOperatorNode) or isinstance(self.left_node, UnaryOperatorNode):
                str_result += self.left_node.stringify(tab_level + 1)
            else:
                str_result += f"[{self.left_node.node_str}]"

        if self.right_node is not None:
            str_result += '\n' + '\t' * (tab_level + 1)
            if isinstance(self.right_node, BinaryOperatorNode) or isinstance(self.right_node, UnaryOperatorNode):
                str_result += self.right_node.stringify(tab_level + 1)
            else:
                str_result += f"[{self.right_node.node_str}]"

        return str_result

    def set_left_node(self, node):
        self.left_node = node
        self.left_node.parent_node = self

    def get_left_node(self):
        return self.left_node

    def set_right_node(self, node):
        self.right_node = node
        self.right_node.parent_node = self

    def get_right_node(self):
        return self.right_node

    def __str__(self):
        return self.stringify(0)


def is_parenthesis(lexeme: str) -> bool:
    return is_left_parenthesis(lexeme) or is_right_parenthesis(lexeme)


def is_left_parenthesis(lexeme: str) -> bool:
    return lexeme == Lexeme.LEFT_PARENTHESIS_LEXEME


def is_right_parenthesis(lexeme: str) -> bool:
    return lexeme == Lexeme.RIGHT_PARENTHESIS_LEXEME


def is_operator(lexeme: str) -> bool:
    return lexeme in operators_prior.keys()


def is_valid_infix_expr(expr: str):
    lexeme_list = tokenize(expr)
    bracket_level: int = 0
    is_operator_lexeme_flag: bool = False

    for t in lexeme_list:
        if is_parenthesis(t):
            if is_left_parenthesis(t) and not is_operator_lexeme_flag: # is_operator_lexeme_flag = false cos after operator parser's waiting for an operand
                bracket_level += 1

                if bracket_level > 256:
                    return False
            elif is_right_parenthesis(t) and is_operator_lexeme_flag:
                bracket_level -= 1

                if bracket_level < 0:
                    return False
            else:
                return False
        elif (t in operators_prior.keys()) != is_operator_lexeme_flag:
            return False
        else:
            is_operator_lexeme_flag = not is_operator_lexeme_flag

    return is_operator_lexeme_flag


def parse_expr(expr: str, bracket_level=0, return_offset=False) -> ExpressionNode or None:
    if bracket_level > 256:
        return None

    expr_offset = 0
    root_node: ExpressionNode or None = None
    current_operator_node: ExpressionNode or None = root_node
    current_operator_right_node: ValueNode or UnaryOperatorNode
    prev_operator_prior: int = -100
    operators_by_prior = dict()
    is_operator_flag = False

    while not ((lexeme_tuple := next_token(expr[expr_offset:])) is None or lexeme_tuple[0] == Lexeme.SEMICOLON_LEXEME):
        token, lexeme_offset = lexeme_tuple
        prev_offset = expr_offset
        expr_offset += lexeme_offset + len(token)

        if is_parenthesis(token):
            if is_left_parenthesis(token):
                if (sub_expr_parse_result := parse_expr(expr[expr_offset:], return_offset=True, bracket_level=bracket_level + 1)) is None:
                    return None

                sub_expression_node, sub_expr_offset = sub_expr_parse_result

                if root_node is None:
                    root_node = sub_expression_node
                else:
                    current_operator_node.set_right_node(sub_expression_node)

                expr_offset += sub_expr_offset + 1
                # after reading subexpression we point to ')', so '+ 1' is for go to the next token

            elif is_right_parenthesis(token):
                bracket_level -= 1

                if bracket_level >= 0:
                    return (root_node, prev_offset) if return_offset else root_node
                else:
                    return None
        elif is_operator(token) and is_operator_flag:
            new_operator_node = BinaryOperatorNode(token)
            new_operator_prior = new_operator_node.get_operator_prior()

            if current_operator_node is None:
                new_operator_node.set_left_node(root_node)
                root_node = new_operator_node
            elif new_operator_prior > prev_operator_prior:
                current_operator_right_node = current_operator_node.get_right_node()
                new_operator_node.set_left_node(current_operator_right_node)
                current_operator_node.set_right_node(new_operator_node)
            elif new_operator_prior == prev_operator_prior:
                if current_operator_node.get_parent_node() is None:
                    root_node = new_operator_node
                else:
                    current_operator_node.get_parent_node().set_right_node(new_operator_node)

                new_operator_node.set_left_node(current_operator_node)
            else:
                conjunct_operator_node = current_operator_node

                while conjunct_operator_node.get_parent_node() is not None and conjunct_operator_node.get_operator_prior() > new_operator_prior:
                    conjunct_operator_node = conjunct_operator_node.get_parent_node()

                if conjunct_operator_node.get_parent_node() is None and conjunct_operator_node.get_operator_prior() > new_operator_prior:
                    root_node = new_operator_node
                    root_node.set_left_node(conjunct_operator_node)
                else:
                    if new_operator_prior > 0 and conjunct_operator_node.get_operator_prior() > 0:
                        if conjunct_operator_node.get_parent_node() is None:
                            root_node = new_operator_node
                        else:
                            conjunct_operator_node.get_parent_node().set_right_node(new_operator_node)

                        new_operator_node.set_left_node(conjunct_operator_node)
                    else:
                        conj_operator_right_node = conjunct_operator_node.get_right_node()
                        new_operator_node.set_left_node(conj_operator_right_node)
                        conjunct_operator_node.set_right_node(new_operator_node)

            current_operator_node = new_operator_node
            prev_operator_prior = new_operator_prior

            if operators_types[token] != 'closed':
                operators_by_prior[new_operator_prior] = new_operator_node

            is_operator_flag = False

        else:
            if is_operator(token) and not is_operator_flag:
                # new_node = UnaryOperatorNode(token)
                # current_operator_node.set_right_node(new_node)
                pass
            else:
                if is_char(token):
                    symbol = token[1:-1]
                    char_token = escape_symbols.get(symbol) if symbol[0] == '\\' else symbol
                    value = ord(char_token)
                elif is_num(token):
                    base = 10

                    if token.startswith('0x'):
                        base = 16
                    elif token.startswith('0b'):
                        base = 2
                    elif token.startswith('0'):
                        base = 8

                    value = int(token[:], base)
                elif token == Lexeme.TRUE_KEYWORD_LEXEME:
                    value = 1
                elif token == Lexeme.FALSE_KEYWORD_LEXEME:
                    value = 0
                else:
                    value = token[:]

                new_value_node = ValueNode(value)

                if root_node is None:
                    root_node = new_value_node
                else:
                    current_operator_node.set_right_node(new_value_node)

                is_operator_flag = True

    result = (root_node, expr_offset) if return_offset else root_node

    return result if bracket_level == 0 else None


main {
    val x;
    get x;

    while (x >= 0) {
        put x;
        get x;
    }
}